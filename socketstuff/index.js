// Express, MongoDB, socket.io
var app = require('express')();
app.disable('x-powered-by'); // to prevent attacks targeted at the framework
var http = require('http').Server(app);
var io = require('socket.io')(http);
var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var mongourl = 'mongodb://localhost:27017/socketstuff';

// used to attach the debugger
var net = require('net');
var repl = require('repl');
var fs = require('fs');

var port = process.env.PORT || 8080;

__dirname = process.cwd()+"/";

var db;
MongoClient.connect(mongourl, function(err, db1){
  assert.equal(null, err);
  db = db1;
});


var socketdata = [];
io.on('connection', function(socket){
  console.log('socket connection:' + socket);
  var socketdatai = {socket: socket, nick: '', room: ''};
  socketdata.push(socketdatai);  

  socket.on('room subscribe', function(msg){
    socketdatai.room = msg.room;
    var cursor = db.collection('chats').find();
    cursor.each(function(err,doc){
      assert.equal(err, null);
      if(doc != null){ // why would this ever be a thing lol
        if(doc.room == msg.room){
          console.log('sending old chats: ' + doc.msg);
          socket.emit('chat message', {msg:  doc.msg,
                                       nick: doc.nick,
                                       time: doc.time});
        }
      }
    });

    console.log('room subscribe: ' + socketdatai.nick + '->' + socketdatai.room);
  });

  socket.on('nick', function(msg){
    var oldnick = socketdatai.nick;
    socketdatai.nick = msg.nick;
    if(socketdatai.room != '' && oldnick != ''){
      roommsg({room: socketdatai.room,
               nick: oldnick,
               msg: "nick change to " + msg.nick,
               time: Date.now()});
    };
    console.log('nick change: ' + oldnick + ' →  ' + msg.nick);
  });


  socket.on('disconnect', function(){
    var nick = socketdatai.nick;
    var room = socketdatai.room;
    if(nick == '')
      nick = 'someone';
    if(room == '')
      room = 'nowhere';
    var time = Date.now();
    var i = socketdata.indexOf(socketdatai);
    socketdata.splice(i,1);
    roommsg({room: room, nick: nick, msg: "disconnected", time: time});
    console.log(nick + ' disconnected from ' + room + " at " + time);
  });

  socket.on('chat message', function(msg){
    smsg = {msg: msg.msg,
            nick: socketdatai.nick,
            room: socketdatai.room,
            time: Date.now()};
    db.collection('chats').insertOne(smsg);
    roommsg(smsg);
    console.log('chat message: ['+smsg.room+']['+smsg.time+']['+smsg.nick+']: '+smsg.msg);
  });

  socket.emit('connected', '');
});

var roommsg = function(msg){
  for(var i=0; i<socketdata.length; i++){
    if(socketdata[i].room == msg.room)
      socketdata[i].socket.emit('chat message', msg);
  }
}

statics = {
  "/": __dirname+"socketstuff/index.html",
  "/turtlepond.js": __dirname+"turtlepond/turtlepond.js",
  "/turtle.png": __dirname+"turtlepond/turtle.png",
  "/fish.png": __dirname+"turtlepond/fish.png",
  "/turtlepond/turtle512.png": __dirname+"turtlepond/turtle512.png",
  "/turtlepond/fish128.png": __dirname+"turtlepond/fish128.png",
  "/domtetris.html": __dirname+"domtetris/domtetris.xhtml",
  "/tetris.css": __dirname+"domtetris/tetris.css",
  "/tetris.js": __dirname+"domtetris/tetris.js",
  "/chatclient.js": __dirname+"socketstuff/chatclient.js",
  "/fractal.html": __dirname+"fractal.html",
  "/fractal-webgl.html": __dirname+"fractal-webgl.html",
  "/fragment.html": __dirname+"fragment.html",
// framework js
  "/socket.io/socket.io.js": __dirname+"socketstuff/node_modules/socket.io-client/socket.io.js"
}
var shaders = ["simple.frag","sprite.frag","circle.frag","gaussian.frag","x2gaussian.frag","wave.frag","pond_wave_display.frag","simple2d.vert","sprite2d.vert"];
for(var i=0; i<shaders.length; i++){
  statics["/shaders/"+shaders[i]] = __dirname+"shaders/"+shaders[i];
}

var sendfile_curry_path = function(path){
  return function(req,res){
    res.sendFile(path);
  }
}
for(s in statics){
  app.get(s, sendfile_curry_path(statics[s]));
  console.log(s+":"+statics[s]);
}

setInterval(function(){
  var lowt = Date.now() - 15 * 60 * 1000;
  db.collection('chats').deleteMany({time: {$lt: lowt}});
},60*1000);


var sockpath = '/tmp/node.sock'
var sockserv = net.createServer(function(socket){
  var therepl = repl.start({prompt:'>',input:socket, output:socket});
  therepl.on('exit',function(){socket.end();});
  therepl.context.db = db;
  therepl.context.socketdata = socketdata;
})
sockserv.on('error', function(e){
  if(e.code == 'EADDRINUSE'){
    fs.unlinkSync(sockpath);
    sockserv.listen(sockpath);
  }
});
sockserv.listen(sockpath);

http.listen(port,function(){
  console.log('listening on *:'+port);
});
